const addQty = (key) => {
    let input =  $(`#quantity-${key}`)
    let qty = parseInt(input.val())
    input.val(++qty)
    SumOrder()
}

const minusQty = (key) => {
    let input =  $(`#quantity-${key}`)
    let qty = parseInt(input.val())
    if(qty == 1){
        alert("Số sản phẩm không thể nhỏ hơn 1")
        input.val(1)
    }else{
        input.val(--qty)
    }
    SumOrder()
}

const del = (key) => {
    let tr = $(`tbody #item-${key}`)
    tr.remove()
}


const SumOrder = () => {
    let items = $("tbody tr")
    let quan = $(".quantity")
    let pricesp = $(".price")
    let discountsp = $('.discount')
    
    let totalPrice = 0;
    let totalDiscount = 0;
    let totalTax =0;
    for(i=1; i<items.length+1; i++){
        let qty = parseInt(quan.eq(i).val())
        let price = parseInt(pricesp.eq(i).text())
        let discount = discountsp.eq(i).text()
        if(discount == "--"){
            discount = 0;
        }else{
            discount = parseInt(discount)
        }

        //  set tax
        let tax = Math.round(qty*price*0.125)
        $(`#tax-${i}`).text(tax.toFixed(2))

        // tinh total
        let total = qty*price - discount + tax
        $(`#total-${i}`).text(total.toFixed(2))

        //  tinh totalPrice, totalDiscount, totalTax
        totalPrice += total
        totalDiscount += discount
        totalTax += tax

    }
    $(`#totalPrice`).text(totalPrice.toFixed(2))
    $(`#totalDiscount`).text(totalDiscount.toFixed(2))
    $(`#totalTax`).text(totalTax.toFixed(2))
}

window.onload = SumOrder()
